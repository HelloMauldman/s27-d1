const http = require("http");

let directory = [
	{
		"name":"Brandon",
		"email":"brandon@gmail.com"
	},
	{
		"name":"Jobert",
		"email":"jobert@gmail.com"
	}
]

const server = http.createServer((req, res) =>{
	//GET all users
	if(req.url == "/users"&&req.method == "GET"){
		res.writeHead(200, {"Content-Type":"text/plain"});

		res.write(JSON.stringify(directory));

		res.end();
		};
	//POST/Add new users
	if(req.url == "/users" && req.method == "POST"){

		let requestBody = "";

		req.on("data", (data)=> {
			requestBody += data;
		});
		req.on("end", ()=>{
			console.log(typeof requestBody);
			requestBody = JSON.parse(requestBody)

			let newUser = {
				"name":requestBody.name,
				"email":requestBody.email
			}
			directory.push(newUser);
			console.log(directory);

			res.writeHead(200, {"Content-Type":"application/json"});
			res.write(JSON.stringify(newUser));
			res.end();	
		});
	};
});
server.listen(3000, "localhost", ()=> {
	console.log("listening to port 4000");
});


